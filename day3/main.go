package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

// Point is a coordinate
type Point struct {
	x, y int
}

func getWires(file string) []string {
	var wirePaths []string

	f, err := os.Open(file)
	if err != nil {
		fmt.Printf("Error opening file: %s", err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		wirePaths = append(wirePaths, scanner.Text())
	}
	return wirePaths
}

func drawWires(wires []string) map[Point]string {
	grid := make(map[Point]string)

	for i := range wires {
		loc := Point{0, 0}
		path := strings.Split(wires[i], ",")
		id := strconv.Itoa(i)
		for n := 0; n < len(path); n++ {
			dir := path[n][0]
			dist, _ := strconv.Atoi(path[n][1:])
			for j := 0; j < dist; j++ {
				switch dir {
				case 'U':
					loc.y++
				case 'D':
					loc.y--
				case 'R':
					loc.x++
				case 'L':
					loc.x--
				default:
					fmt.Println("unknown direction")
				}
				if grid[loc] == id {
					continue
				}
				grid[loc] = grid[loc] + id
			}
		}
	}
	return grid
}

func findIntersect(grid map[Point]string) int {
	var points []Point
	var distances []int
	for i := range grid {
		if len(grid[i]) > 1 {
			points = append(points, i)
		}
	}
	for _, i := range points {
		distance := abs(i.x) + abs(i.y)
		distances = append(distances, distance)
	}
	sort.Ints(distances)
	return distances[0]
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func main() {

	wires := getWires("input.txt")
	grid := drawWires(wires)
	intersect := findIntersect(grid)

	fmt.Println(intersect)
}
