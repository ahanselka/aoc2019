package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func calcFuel(mass int) int {
	var fuel int
	var totalFuel int
	if mass <= 0 {
		return 0
	}
	fuel = (mass / 3) - 2
	if fuel > 0 {
		totalFuel += fuel + calcFuel(fuel)
	}
	return totalFuel
}
func main() {
	var totalFuel int

	input, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer input.Close()
	scanner := bufio.NewScanner(input)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		mass, _ := strconv.Atoi(scanner.Text())
		totalFuel += calcFuel(mass)
	}
	fmt.Println(totalFuel)
}
