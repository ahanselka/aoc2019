package main

func runOpcode(o int, input1 int, input2 int) (int, error) {
	var err error
	switch o {
	case 1:
		return input1 + input2, err
	case 2:
		return input1 * input2, err
	case 3:

	case 99:
		return 0, err
	default:
		return 0, err
	}
}
