package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func generateSlice(d []string) []int {
	var opcodes []int

	for _, code := range d {
		i, _ := strconv.Atoi(code)
		opcodes = append(opcodes, i)
	}

	return opcodes
}

func runOpcode(o int, input1 int, input2 int) (int, error) {
	var err error
	switch o {
	case 1:
		return input1 + input2, err
	case 2:
		return input1 * input2, err
	case 99:
		return 0, err
	default:
		return 0, err
	}
}

func main() {
	var opcodes []int
	input, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer input.Close()
	data, _ := bufio.NewReader(input).ReadString('\n')
	batch := 4
	for n := 0; n < 100; n++ {
		for x := 0; x < 100; x++ {
			opcodes = generateSlice(strings.Split(data, ","))
			opcodes[1] = n
			opcodes[2] = x
			for i := 0; i < len(opcodes); i += batch {
				j := i + batch
				if j > len(opcodes) {
					j = len(opcodes)
					break
				}
				input1 := opcodes[i+1]
				input2 := opcodes[i+2]
				resultLocation := opcodes[i+3]

				answer, err := runOpcode(opcodes[i], opcodes[input1], opcodes[input2])
				if err != nil {
					fmt.Println("oopsies")
					os.Exit(1)
				}

				opcodes[resultLocation] = answer
			}
			if opcodes[0] == 19690720 {
				fmt.Println(100*n + x)
			}
		}
	}
	fmt.Println(opcodes[0])
}
